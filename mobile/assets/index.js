import ReactDOM from "https://cdn.skypack.dev/react-dom";
import React, { useRef, useState } from "https://cdn.skypack.dev/react";
// import { Canvas, useFrame } from "https://cdn.skypack.dev/@react-three/fiber";
import htm from "https://cdn.skypack.dev/htm";

const html = htm.bind(React.createElement);

// function Box(props) {
//   const mesh = useRef();
//   const [hovered, setHover] = useState(false);
//   const [active, setActive] = useState(false);
//   useFrame(() => (mesh.current.rotation.x = mesh.current.rotation.y += 0.01));
//   return html` <mesh
//     ...${props}
//     ref=${mesh}
//     scale=${active ? 1.5 : 1}
//     onClick=${() => setActive(!active)}
//     onPointerOver=${() => setHover(true)}
//     onPointerOut=${() => setHover(false)}
//   >
//     <boxGeometry args=${[1, 1, 1]} />
//     <meshStandardMaterial color=${hovered ? "hotpink" : "orange"} />
//   </mesh>`;
// }

ReactDOM.render(
  html`<div>This is React</div>`,
  document.getElementById("root")
);
