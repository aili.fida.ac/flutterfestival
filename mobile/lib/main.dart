import 'package:flutter/material.dart';
import 'package:flutterfestival/src/app.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(const MyApp());
}
