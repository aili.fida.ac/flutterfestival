import 'package:flutter/material.dart';
import 'package:webview_flutter_plus/webview_flutter_plus.dart';

class WebView extends StatefulWidget {
  const WebView({Key? key}) : super(key: key);

  @override
  State<WebView> createState() => _WebViewState();
}

class _WebViewState extends State<WebView> {
  bool isLoading = false;
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: WebViewPlus(
        onProgress: (progress) {
          print(progress);
        },
        onWebViewCreated: ((controllerPlus) =>
            controllerPlus.loadUrl('assets/index.html')),
        onPageFinished: (url) {},
        javascriptMode: JavascriptMode.unrestricted,
      ),
    );
  }
}
